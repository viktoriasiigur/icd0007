<?php
require_once "PersonItem.php";

const USERNAME = 'viktoriasiigur';
const PASSWORD = '13fc';
const URL = "mysql:host=db.mkalmo.xyz;dbname=viktoriasiigur";


function addPersonItem($personItem) {
    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare("insert into viktoriasiigur.contacts(first_name, last_name) 
        values (:first_name, :last_name)");
    $statement->bindValue(":first_name", $personItem->firstName);
    $statement->bindValue(":last_name", $personItem->lastName);

    $statement->execute();
    $contactId = $connection->lastInsertId();

    foreach ($personItem->phones as $phone) {
        $statement = $connection->prepare(
            "insert into viktoriasiigur.phones (contact_id, number) values(:contactId, :number)");
        $statement->bindValue(":contactId", $contactId);
        $statement->bindValue(":number", $phone);
        $statement->execute();
    }
}

function getPersonItems() {
    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare("select c.id, c.first_name, c.last_name, p.number 
    from viktoriasiigur.contacts c left join viktoriasiigur.phones p on p.contact_id = c.id;");
    $statement->execute();

    $personItems = [];
    foreach ($statement as $row) {
        $id = $row["id"];

        if (isset($personItems[$id])) {
            $personItem = $personItems[$id];
            $phone = $row["number"];
            $personItem->addPhone($phone);
        } else {
            $firstName = $row["first_name"];
            $lastName = $row["last_name"];
            $phone = $row["number"];
            $personItem = new PersonItem($firstName, $lastName, $id);
            $personItem->addPhone($phone);
        }

        $personItems[$id] = $personItem;
    }

    return array_values($personItems);
}

function deletePersonItem($personId) {
    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare(
        "delete from viktoriasiigur.contacts where id = :id;");
    $statement->bindValue(':id', $personId);
    $statement->execute();

    $statement = $connection->prepare(
        "delete from viktoriasiigur.phones where contact_id = :id;");
    $statement->bindValue(':id', $personId);
    $statement->execute();

}