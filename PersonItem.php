<?php


class PersonItem
{
    public $id;
    public $firstName;
    public $lastName;
    public $phones;

    public function __construct($firstName = "", $lastName = "", $id = null) {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->phones = [];
    }
    public function addPhone($phone) {
        $this->phones[] = $phone;
    }

    public function validate() {
        $errors = [];

        if (strlen($this->firstName) < 2 || strlen($this->firstName) > 50){
            $errors[] = "Eesnimi peab olema 2 kuni 50 tähemärki";
        }

        if (strlen($this->lastName) < 2 || strlen($this->lastName) > 50) {
            $errors[] = "Perekonnanimi peab olema 2 kuni 50 tähemärki";
        }

        return $errors;
    }
}