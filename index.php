<?php
require_once("vendor/tpl.php");
require_once("PersonItem.php");
require_once("personData.php");

if (isset($_GET['command'])){
    $command = $_GET['command'];
}
else {
    $command = 'show_list_page';
}

$data = [];

if ($command === "show_list_page") {
    $data['subTemplate'] = 'list.html';
    $data['personItems'] = getPersonItems();
    print renderTemplate("templates/main.html", $data);

}
else if ($command === "save") {
    $firstName = urlencode($_POST["firstName"]);
    $lastName = urlencode($_POST["lastName"]);
    $phone1 = urlencode($_POST["phone1"]);
    $phone2 = urlencode($_POST["phone2"]);
    $phone3 = urlencode($_POST["phone3"]);
    $personItem = new PersonItem($firstName, $lastName);
    $personItem->addPhone($phone1);
    $personItem->addPhone($phone2);
    $personItem->addPhone($phone3);
    $errors = $personItem->validate();
    if (!empty($errors)){
        $data['subTemplate'] = 'add.html';
        $data['personItem'] = $personItem;
        $data['errors'] = $errors;
        print renderTemplate("templates/main.html", $data);
    }
    else {
        addPersonItem($personItem);
        $data['subTemplate'] = 'list.html';
        header("Location: index.php?command=show_list_page");
    }
} else if ($command === "edit") {
    $id = $_GET['id'];
    $personItems = getPersonItems();
    $personItem = new PersonItem();
    foreach ($personItems as $item) {
        if ($item->id == $id){
            $personItem = $item;
        }
    }
    deletePersonItem($id);
    $data['subTemplate'] = 'add.html';
    $data['personItem'] = $personItem;
    print renderTemplate("templates/main.html", $data);
}


else {
    $data['subTemplate'] = 'add.html';
    print renderTemplate("templates/main.html", $data);
}